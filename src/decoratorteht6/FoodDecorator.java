/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decoratorteht6;

/**
 *
 * @author aleks
 */
public abstract class FoodDecorator implements Pizza{  
    private PizzaPohja newPizza;  
    public FoodDecorator(PizzaPohja newPizza)  {  
        this.newPizza=newPizza;  
    }  
    public String prepareFood(){  
        return newPizza.preparePizza();   
    }  
    public double foodPrice(){  
        return newPizza.PizzaPrice();  
    }  
}  
