/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decoratorteht6;

/**
 *
 * @author aleks
 */
public class FantasiaAnanas extends FoodDecorator{
        
        public FantasiaAnanas(PizzaPohja newPizza) {
        super(newPizza);
    }
 
      
    public String preparePizza(){  
        return super.prepareFood() +"Ananas Lisätty";   
    }  
    public double PizzaPrice()   {  
        return super.foodPrice()+0.0;  
    }  
    
}
