/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decoratorteht6;

/**
 *
 * @author aleks
 */
public class SeafoodPizza extends FoodDecorator{  

    public SeafoodPizza(PizzaPohja newPizza) {
        super(newPizza);
    }
    public String preparePizza(){  
        return super.prepareFood() +" With Fried Rice and Manchurian  ";   
    }  
    public double PizzaPrice()   { 
        System.out.println("Seasfood Hinta 8 euroa");
        return super.foodPrice()+8.0;  
        }  

}  
